#   Copyright 2021 Google LLC
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

terraform {
  required_version = ">= 1.0.0"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 4.0.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.5.0"
    }
    null = {
      source  = "hashicorp/null"
      version = ">= 3.1.0"
    }
  }
}

# Some firewall rules required for Sonobuoy's mutating webhooks and other things
module "sonobuoy-firewall" {
  source = "github.com/terraform-google-modules/cloud-foundation-fabric//modules/net-vpc-firewall?ref=v8.0.0"

  project_id = var.project_id
  network    = var.network_name

  admin_ranges        = []
  ssh_source_ranges   = []
  http_source_ranges  = []
  https_source_ranges = []

  custom_rules = {
    allow-sonobuoy-webhooks = {
      description          = "Allow Sonobuoy traffic for compliance testing."
      direction            = "INGRESS"
      action               = "allow"
      sources              = []
      ranges               = [var.cidr_range_controlplane]
      targets              = ["sonobuoy"]
      use_service_accounts = false
      priority             = 1000
      rules                = [{ protocol = "tcp", ports = ["1-65535"] }]
      logging              = "INCLUDE_ALL_METADATA"
      extra_attributes     = {}
    }
  }
}

# Required for Sonobuoy: https://github.com/vmware-tanzu/sonobuoy#run-on-google-cloud-platform-gcp
resource "kubernetes_cluster_role_binding" "sonobuoy-clusterrole-binding" {
  count    = var.compliance_testing_use ? 1 : 0
  provider = kubernetes

  metadata {
    name = "sonobuoy-clusteradmin"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "User"
    name      = var.admin_email
    api_group = "rbac.authorization.k8s.io"
  }
}

# Patch admission controller webhook side effects to None (instead of Unknown)
# (works around issue with dryrun and admission controlliing)
resource "null_resource" "admission-controller-patch" {
  triggers = {
    endpoint = var.kubernetes_endpoint
    ca_crt   = var.ca_certificate
    token    = var.access_token
  }

  provisioner "local-exec" {
    command = <<EOH
cat >$${TMPDIR:-/tmp}/ca.crt <<EOF
${var.ca_certificate}
EOF
  kubectl \
  --server="${var.kubernetes_endpoint}" \
  --token="${var.access_token}" \
  --certificate-authority=$${TMPDIR:-/tmp}/ca.crt \
  patch validatingwebhookconfiguration.admissionregistration.k8s.io \
  binauthz-admission-controller \
  --type json -p='[{"op": "add", "path": "/webhooks/0/sideEffects", "value":"None"}]'
EOH
  }
}

