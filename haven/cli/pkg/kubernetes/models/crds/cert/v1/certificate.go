package v1

import "encoding/json"

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    certificateResource, err := UnmarshalCertificateResource(bytes)
//    bytes, err = certificateResource.Marshal()

func UnmarshalCertificateResource(data []byte) (CertificateResource, error) {
	var r CertificateResource
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *CertificateResource) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

// CertificateResource is a custom model for unstructured kube objects such as CRDs
type CertificateResource struct {
	Object Object `json:"Object"`
}

type Object struct {
	APIVersion string   `json:"apiVersion"`
	Kind     string   `json:"kind"`
	Metadata Metadata `json:"metadata"`
	Spec     Spec     `json:"spec"`
	Status   Status   `json:"status"`
}

type Metadata struct {
	CreationTimestamp string `json:"creationTimestamp"`
	Generation        int64  `json:"generation"`
	Name              string `json:"name"`
	Namespace         string `json:"namespace"`
	ResourceVersion   string `json:"resourceVersion"`
	Uid               string `json:"uid"`
}

type Spec struct {
	DNSNames   []string  `json:"dnsNames"`
	IssuerRef  IssuerRef `json:"issuerRef"`
	SecretName string    `json:"secretName"`
	Usages     []string  `json:"usages"`
}

type IssuerRef struct {
	Group string `json:"group"`
	Kind  string `json:"kind"`
	Name  string `json:"name"`
}

type Status struct {
	Conditions  []Condition `json:"conditions"`
	NotAfter    string      `json:"notAfter"`
	NotBefore   string      `json:"notBefore"`
	RenewalTime string      `json:"renewalTime"`
	Revision    int64       `json:"revision"`
}

type Condition struct {
	LastTransitionTime string `json:"lastTransitionTime"`
	Message            string `json:"message"`
	ObservedGeneration int64  `json:"observedGeneration"`
	Reason             string `json:"reason"`
	Status             string `json:"status"`
	Type               string `json:"type"`
}
