package kubernetes

import (
	"context"
	"fmt"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/models/crds/haven/v1alpha"
	extv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"time"
)

type V1Alpha1 interface {
	List() (*v1alpha.CompliancyList, error)
	Get(name string) (*v1alpha.Compliancy, error)
	Create(compliancy *v1alpha.Compliancy) (*v1alpha.Compliancy, error)
	CreateHavenResource(output string, input v1alpha.Input, compliant bool) (*v1alpha.Compliancy, error)
}

type V1Alpha1Client struct {
	Client rest.Interface
}

func NewCrdConfig(c *rest.Config) (V1Alpha1, error) {
	config := *c
	config.ContentConfig.GroupVersion = &schema.GroupVersion{Group: v1alpha.GroupName, Version: v1alpha.Version}
	config.APIPath = "/apis"
	config.NegotiatedSerializer = scheme.Codecs.WithoutConversion()
	config.UserAgent = rest.DefaultKubernetesUserAgent()

	client, err := rest.RESTClientFor(&config)
	if err != nil {
		return nil, err
	}

	return &V1Alpha1Client{Client: client}, nil
}

func (c *V1Alpha1Client) CreateHavenResource(output string, input v1alpha.Input, compliant bool) (*v1alpha.Compliancy, error) {
	out, err := v1alpha.UnmarshalOutputResource([]byte(output))
	if err != nil {
		return nil, err
	}

	apiVersion := fmt.Sprintf("%s/%s", v1alpha.GroupName, v1alpha.Version)

	compliancyResource := v1alpha.Compliancy{
		TypeMeta: metav1.TypeMeta{},
		ObjectMeta: metav1.ObjectMeta{
			Name: fmt.Sprintf("haven-%s", time.Now().UTC().Format("20060102-150405")),
		},
		APIVersion: apiVersion,
		Kind:       v1alpha.Kind,
		Spec: v1alpha.Spec{
			Compliant: compliant,
			Created:   time.Now().UTC().String(),
			Version:   fmt.Sprintf("Haven %s", v1alpha.Version),
			Output:    out,
			Input:     input,
		},
	}

	return &compliancyResource, nil
}

func (c *V1Alpha1Client) Create(compliancy *v1alpha.Compliancy) (*v1alpha.Compliancy, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	compl, err := compliancy.Marshal()
	if err != nil {
		return nil, err
	}

	result := v1alpha.Compliancy{}
	err = c.Client.
		Post().
		Resource(v1alpha.Plural).
		Body(compl).
		Do(ctx).
		Into(&result)

	return &result, err
}

func (c *V1Alpha1Client) List() (*v1alpha.CompliancyList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	result := v1alpha.CompliancyList{}
	err := c.Client.
		Get().
		Resource(v1alpha.Plural).
		Do(ctx).
		Into(&result)

	return &result, err
}

func (c *V1Alpha1Client) Get(name string) (*v1alpha.Compliancy, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	result := v1alpha.Compliancy{}
	err := c.Client.
		Get().
		Name(name).
		Resource(v1alpha.Plural).
		Do(ctx).
		Into(&result)

	return &result, err
}

func (k *KubeImpl) ListCustomResourceDefinitions() (*extv1.CustomResourceDefinitionList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Minute)
	defer cancel()

	crds, err := k.GetK8sExtensionClient().ApiextensionsV1().CustomResourceDefinitions().List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	return crds, nil
}

func (k *KubeImpl) DeleteCustomResourceDefinition(name string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	err := k.GetK8sExtensionClient().ApiextensionsV1().CustomResourceDefinitions().Delete(ctx, name, metav1.DeleteOptions{})

	return err
}

func (k *KubeImpl) DeleteCustomResource(group, version, resource, namespace, name string) error {
	c, err := dynamic.NewForConfig(k.GetK8sRestConfig())
	if err != nil {
		return fmt.Errorf("Could not create dynamic Kubernetes client: %s", err.Error())
	}

	err = c.Resource(schema.GroupVersionResource{Group: group, Version: version, Resource: resource}).Namespace(namespace).Delete(context.Background(), name, metav1.DeleteOptions{})
	return err
}

func (k *KubeImpl) GetCustomResource(group, version, resource, namespace, name string) (*unstructured.Unstructured, error) {
	c, err := dynamic.NewForConfig(k.GetK8sRestConfig())
	if err != nil {
		return nil, fmt.Errorf("Could not create dynamic Kubernetes client: %s", err.Error())
	}

	rsc, err := c.Resource(schema.GroupVersionResource{Group: group, Version: version, Resource: resource}).Namespace(namespace).Get(context.Background(), name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	return rsc, nil
}

func (k *KubeImpl) ListCustomResources(group, version, resource string) (*unstructured.UnstructuredList, error) {
	c, err := dynamic.NewForConfig(k.GetK8sRestConfig())
	if err != nil {
		return nil, fmt.Errorf("Could not create dynamic Kubernetes client: %s", err.Error())
	}

	resources, err := c.Resource(schema.GroupVersionResource{Group: group, Version: version, Resource: resource}).List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	return resources, nil
}
