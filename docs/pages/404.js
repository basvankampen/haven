// Copyright © VNG Realisatie 2019-2021
// Licensed under the EUPL
//
import { getPageSections } from 'src/lib/api'
import NotFound from 'src/pages/NotFound'

export default function NotFoundPage(props) {
  return <NotFound {...props} />
}

export async function getStaticProps() {
  const sections = await getPageSections('not-found')
  return { props: { sections } }
}
