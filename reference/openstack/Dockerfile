# Copyright © VNG Realisatie 2019-2021
# Licensed under the EUPL

#####################################################################################################################################

##### [ HAVEN REFERENCE IMPLEMENTATION: OPENSTACK ] #####
FROM        alpine

ARG         KOPS_VERSION=1.20.2
ARG         OPENSTACK_CLOUD_CONTROLLER_MANAGER_VERSION=1.20.2
ARG         KUBECTL_VERSION=1.20.9
ARG         HELM_VERSION=3.6.3
ARG         SONOBUOY_VERSION=0.52.0

# Exports used by provisioners.
ENV         OPENSTACK_CLOUD_CONTROLLER_MANAGER_VERSION=${OPENSTACK_CLOUD_CONTROLLER_MANAGER_VERSION}
ENV         KUBECTL_VERSION=${KUBECTL_VERSION}
ENV         METRICS_CHART_VERSION=2.11.4

# [ Base ]
RUN         mkdir -p /opt/haven
ENV         HOME=/opt/haven
WORKDIR     /opt/haven

RUN         apk add --no-cache bash bash-completion vim curl openssh-client build-base git python3 python3-dev py3-pip jq gcc openssl-dev libffi-dev musl-dev cargo apache2-utils
RUN         pip3 install --upgrade pip
RUN         mkdir -p /opt/haven/bin/helpers

# [ CLI ]
# Openstack.
RUN         pip3 install python-openstackclient==5.4.0 python-neutronclient==7.3.0 python-octaviaclient==2.3.0 python-swiftclient==3.11.1
# Kubernetes..
RUN         (curl -L -o /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl && chmod 755 /usr/local/bin/kubectl)
# Helm.
RUN         (curl -L https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz | tar -xzO linux-amd64/helm > /usr/local/bin/helm && chmod 755 /usr/local/bin/helm)
RUN         helm repo add stable https://charts.helm.sh/stable
RUN         helm repo add appscode https://charts.appscode.com/stable/
RUN         helm repo update

# [ Kops ]
RUN         (curl -L -o /usr/local/bin/kops https://github.com/kubernetes/kops/releases/download/v${KOPS_VERSION}/kops-linux-amd64 && chmod 755 /usr/local/bin/kops)
COPY        kops/bin/ /opt/haven/bin/

# [ Bash ]
RUN         ln -s /opt/haven/bin/helpers/_bashrc /opt/haven/.bashrc
RUN         /usr/local/bin/kops completion bash >> /opt/haven/.bashrc
RUN         /usr/local/bin/kubectl completion bash >> /opt/haven/.bashrc
RUN         /usr/local/bin/helm completion bash >> /opt/haven/.bashrc
RUN         /usr/bin/openstack complete >> /opt/haven/.bashrc

# [ Secrets ]
RUN         pip3 install ansible==3.1.0

# [ Haven CLI ]
COPY        haven /opt/haven/bin/haven
RUN         (curl -L https://github.com/vmware-tanzu/sonobuoy/releases/download/v${SONOBUOY_VERSION}/sonobuoy_${SONOBUOY_VERSION}_linux_amd64.tar.gz | tar -xzO sonobuoy > /usr/local/bin/sonobuoy && chmod 755 /usr/local/bin/sonobuoy)

# [ Base ]
COPY        docker-entrypoint.sh /docker-entrypoint.sh
RUN         chmod 755 /docker-entrypoint.sh

# Export used by _bashrc.
ARG         HAVEN_VERSION=undefined
ENV         HAVEN_VERSION=${HAVEN_VERSION}

ENTRYPOINT  ["/docker-entrypoint.sh"]
