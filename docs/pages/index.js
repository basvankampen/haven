// Copyright © VNG Realisatie 2019-2021
// Licensed under the EUPL
//

import { getPageSections } from 'src/lib/api'
import Home from '../src/pages/Home'

export default function HomePage(props) {
  return <Home {...props} />
}

export async function getStaticProps() {
  const sections = await getPageSections('home')
  return { props: { sections } }
}
