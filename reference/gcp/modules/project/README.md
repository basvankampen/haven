# Project

Sets up a GCP project by activating APIs necessary for rest of the resources.

<!-- BEGIN TFDOC -->
## Variables

| name | description | type | required | default |
|---|---|:---: |:---:|:---:|
| project_id | GCP project ID | <code title="">string</code> | ✓ |  |
| *activate_apis* | List of APIs to activate in the GCP project | <code title="list&#40;string&#41;">list(string)</code> |  | <code title="&#91;&#10;&#34;compute.googleapis.com&#34;,&#10;&#34;container.googleapis.com&#34;,&#10;&#34;gkehub.googleapis.com&#34;,&#10;&#34;file.googleapis.com&#34;,&#10;&#34;anthos.googleapis.com&#34;,&#10;&#34;stackdriver.googleapis.com&#34;,&#10;&#34;monitoring.googleapis.com&#34;,&#10;&#34;logging.googleapis.com&#34;,&#10;&#34;binaryauthorization.googleapis.com&#34;,&#10;&#34;anthosconfigmanagement.googleapis.com&#34;,&#10;&#34;cloudkms.googleapis.com&#34;&#10;&#93;">...</code> |

## Outputs

| name | description | sensitive |
|---|---|:---:|
| project_id | None |  |
| project_number | None |  |
<!-- END TFDOC -->
