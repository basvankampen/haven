package kubernetes

import (
	"context"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"time"
)

// ListPods gets a list of pods from a ns - empty string will be treated as all
func (k *KubeImpl) ListPods(namespace string) (*corev1.PodList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	pods, err := k.GetK8sClientSet().CoreV1().Pods(namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	return pods, nil
}

// GetPodByName will retrieve a pod from a namespace by using the name of the pod
func (k *KubeImpl) GetPodByName(namespace, podName string) (*corev1.Pod, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	namedPod, err := k.GetK8sClientSet().CoreV1().Pods(namespace).Get(ctx, podName, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	return namedPod, nil
}

// DeletePod will delete a named pod
func (k *KubeImpl) DeletePod(namespace, podName string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	err := k.GetK8sClientSet().CoreV1().Pods(namespace).Delete(ctx, podName, metav1.DeleteOptions{})

	return err
}
