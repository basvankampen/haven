# Dashboard

The Haven Dashboard allows administrators to manage Helm charts on their cluster from a user-friendly user interface.

Haven Dashboard is part of the Community addons and is not required for Haven Compliancy.

![Screenshot of the Haven Dashboard](/uploads/852c5c4d02bbbda7e8ee6897f19aac46/image.png)

## Using the dashboard

See [../../haven/cli/README.md](../cli/README.md) for instructions how to use the dashboard.

## Setup development environment

First install the [FluxCD toolkit](https://toolkit.fluxcd.io/) with:

```bash
curl -O https://toolkit.fluxcd.io/install.sh
```

Check the contents of the install.sh and then install Flux on your local machine with:

```bash
sudo bash install.sh
```

Now install Flux on the cluster with:

```bash
flux install
```

Start a proxy to the Kubernetes API with:

```bash
kubectl proxy
```

Finally use the following commands to start the UI:

```bash
npm install
npm start
```

Go to https://localhost:3000/ to view the dashboard and start developing.

## License
Copyright © VNG Realisatie 2019-2021
Licensed under the EUPL
