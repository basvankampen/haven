variable "enable_fluentd" {
  description = "Enable fluentd daemon on AKS cluster"
  type        = bool
  default     = true
}

variable "fluentd_namespace" {
  description = ""
  type        = string
  default     = "fluentd"
}

variable "fluentd_chart_repository" {
  description = "Helm chart repository URL"
  type        = string
  default     = "https://weaveworks.github.io/fluentd"
}

variable "fluentd_chart_version" {
  description = "Version of the Helm chart"
  type        = string
  default     = "2.2.0"
}

variable "fluentd_settings" {
  description = ""
  type        = map(string)
  default     = {}
}
