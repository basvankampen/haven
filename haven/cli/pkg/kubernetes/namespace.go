package kubernetes

import (
	"context"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	corev1 "k8s.io/api/core/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"time"
)

// CreateNamespace checks if a namespace exists and creates one if it does not exist yet
func (k *KubeImpl) CreateNamespace(namespace string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	ns, err := k.GetK8sClientSet().CoreV1().Namespaces().Get(ctx, namespace, metav1.GetOptions{})
	if err != nil {
		if k8serrors.IsNotFound(err) {
			logging.Info("Namespace %s not found, creating", namespace)
			nsToCreate := &corev1.Namespace{
				TypeMeta: metav1.TypeMeta{Kind: "Namespace", APIVersion: "v1"},
				ObjectMeta: metav1.ObjectMeta{
					Name: namespace,
				},
			}
			ns, err = k.GetK8sClientSet().CoreV1().Namespaces().Create(ctx, nsToCreate, metav1.CreateOptions{})
			if err != nil {
				return err
			}

			logging.Info("Created namespace %s", ns.Name)
		}
	} else {
		logging.Info("Namespace %s already exists", ns.Name)
	}

	return err
}

func (k *KubeImpl) DeleteNamespace(namespace string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Minute)
	defer cancel()

	err := k.GetK8sClientSet().CoreV1().Namespaces().Delete(ctx, namespace, metav1.DeleteOptions{})

	return err
}
