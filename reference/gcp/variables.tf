#   Copyright 2021 Google LLC
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

variable "project_id" {
  type        = string
  description = "GCP project ID"
}

variable "impersonate_service_account" {
  type        = string
  description = "Impersonate a service account"
  default     = ""
}

variable "admin_email" {
  type        = string
  description = "Username that you are authenticated under"
}

variable "region" {
  type        = string
  description = "Region to deploy GKE into"
  default     = "europe-west4"
}

variable "network" {
  type        = string
  description = "VPC name"
  default     = "haven-compliance"
}

# Sample IP plan:
# Summarization for all cloud resources: 192.168.165.0/19 = 8192 IPs 
# 
# GKE pod range:     192.168.128.0/20  = 4096 IPs (up to 32 nodes with 64 pods per node)
# Services range:    192.168.160.0/23  = 512 IPs  (up to 512 services)
# L7 ILBs:           192.168.162.0/23  = 512 IPs  (recommended subnet size)
# Nodes:             192.168.164.0/24  = 256 IPs  (up to 254 instances)
# GKE control plane: 192.168.165.0/28  = 16 IPs   (required by GKE)
# Filestore:         192.168.165.16/29 = 8 IPs    (required by Filestore)

variable "cidr_range" {
  type        = string
  description = "Node subnet CIDR"
  default     = "192.168.164.0/24"
}

variable "cidr_range_pods" {
  type        = string
  description = "GKE pod CIDR"
  default     = "192.168.128.0/20"
}

variable "cidr_range_ilb" {
  type        = string
  description = "Internal HTTP(S) load balancing subnet range"
  default     = "192.168.162.0/23"
}

variable "cidr_range_services" {
  type        = string
  description = "GKE services CIDR"
  default     = "192.168.160.0/23"
}

variable "cidr_range_filestore" {
  type        = string
  description = "Filestore CIDR"
  default     = "192.168.165.16/29"
}

variable "cidr_range_controlplane" {
  type        = string
  description = "GKE control plane range (/28)"
  default     = "192.168.165.0/28"
}

variable "cluster_name" {
  type        = string
  description = "GKE cluster name"
  default     = "haven-compliance-testing"
}

variable "cluster_instance_type" {
  type        = string
  description = "GKE cluster instance type"
  default     = "e2-standard-8"
}

variable "nfs_share_name" {
  type        = string
  description = "NFS share name"
  default     = "haven"
}

variable "compliance_testing_use" {
  type        = bool
  description = "Set to true if this cluster is being used for compliance testing"
  default     = true
}

variable "deploy_anthos_policy_controller" {
  type        = bool
  description = "Deployed managed Anthos Policy Controller with CIS policies"
  default     = true
}

variable "deploy_cis_policies" {
  type        = bool
  description = "Deploy sample CIS policies for Anthos Policy Controller"
  default     = true
}

variable "enable_binary_authorization" {
  type        = bool
  description = "Enable binary authorization in cluster"
  default     = true
}

variable "deploy_example_workload" {
  type        = bool
  description = "Deploy example Hello World nginx"
  default     = true
}
