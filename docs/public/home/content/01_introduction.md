---
siteTitle: Een standaard voor platform-onafhankelijke cloud hosting.
siteSubTitle: Een applicatie of website die op één Haven omgeving werkt, is herbruikbaar op álle Haven omgevingen.
videoCaption: Haven in één minuut.
videoUriWebm: /home/content/haven.webm
videoUriMp4: /home/content/haven.mp4
videoStill: /home/content/haven-video-still.jpg
fallbackYoutubeText: Uw browser ondersteund het afspelen van video niet. Bekijk deze op
fallbackYoutubeUri: https://www.youtube.com/watch?v=405lA6tJgzI
referenceText: "Met referentie implementaties van:"
referenceUrl: "/techniek/aan-de-slag"
referenceUrlText: "Bekijk meer"
organizationImages:
  - src: home/content/microsoft.svg
    alt: Microsoft
  - src: home/content/google.svg
    alt: Google
  - src: home/content/vmware.svg
    alt: VMWare
  - src: home/content/amazon.svg
    alt: Amazon
  - src: home/content/red-hat.svg
    alt: Red Hat
---
