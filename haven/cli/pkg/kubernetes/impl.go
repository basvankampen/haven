package kubernetes

import (
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	"io/ioutil"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	extv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	extensionsclient "k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/cli-runtime/pkg/genericclioptions"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"os"
	"path/filepath"
)

// Kube interface creates a client to interact with k8s
type Kube interface {
	CreateNamespace(namespace string) error
	DeleteNamespace(namespace string) error
	CreateSecret(namespace, secretName, typeAsString string, data map[string][]byte) error
	GetSecret(namespace, secretName string) (*corev1.Secret, error)
	ListSecrets(namespace string) (*corev1.SecretList, error)
	DeleteSecret(namespace, secretName string) error
	GetService(namespace, serviceName string) (*corev1.Service, error)
	ListServices(namespace string) (*corev1.ServiceList, error)
	ListPods(namespace string) (*corev1.PodList, error)
	GetPodByName(namespace, podName string) (*corev1.Pod, error)
	DeletePod(namespace, podName string) error
	ListCustomResourceDefinitions() (*extv1.CustomResourceDefinitionList, error)
	DeleteCustomResourceDefinition(name string) error
	DeleteCustomResource(group, version, resource, namespace, name string) error
	GetCustomResource(group, version, resource, namespace, name string) (*unstructured.Unstructured, error)
	ListCustomResources(group, version, resource string) (*unstructured.UnstructuredList, error)
	ListJobs(namespace string) (*batchv1.JobList, error)
	DeleteJob(namespace, jobName string) error
	ListVolumesClaims(namespace string) (*corev1.PersistentVolumeClaimList, error)
	DeleteVolumesClaim(namespace, name string) error
}

// KubeImpl can be used to interact with the different kubernetes clients
type KubeImpl struct {
	ClientSet        *kubernetes.Clientset
	ExtensionsClient *extensionsclient.Clientset
	RestConfig       *rest.Config
	config           []byte
}

func (k *KubeImpl) GetK8sClientSet() *kubernetes.Clientset {
	return k.ClientSet
}

func (k *KubeImpl) GetK8sExtensionClient() *extensionsclient.Clientset {
	return k.ExtensionsClient
}

func (k *KubeImpl) GetK8sRestConfig() *rest.Config {
	return k.RestConfig
}

func (k *KubeImpl) GetImpl() *KubeImpl {
	return k
}

func (k *KubeImpl) KubeCliConfig(namespace string) (*genericclioptions.ConfigFlags, error) {
	kubeConfig := genericclioptions.NewConfigFlags(false)
	kubeConfig.APIServer = &k.RestConfig.Host
	kubeConfig.BearerToken = &k.RestConfig.BearerToken
	kubeConfig.CAFile = &k.RestConfig.CAFile
	kubeConfig.Namespace = &namespace

	return kubeConfig, nil
}

func NewClientFromFile(kubeConfig []byte) (Kube, error) {
	client, err := New(kubeConfig)
	if err != nil {
		logging.Error("Error getting creating client")
		return nil, err
	}

	return client, nil
}

func New(config []byte) (*KubeImpl, error) {
	c, err := clientcmd.NewClientConfigFromBytes(config)
	if err != nil {
		return nil, err
	}

	restConfig, err := c.ClientConfig()
	if err != nil {
		return nil, err
	}

	clientSet, err := kubernetes.NewForConfig(restConfig)
	if err != nil {
		return nil, err
	}

	extensionsClient, err := extensionsclient.NewForConfig(restConfig)
	if err != nil {
		return nil, err
	}

	return &KubeImpl{
			ClientSet:        clientSet,
			ExtensionsClient: extensionsClient,
			RestConfig:       restConfig,
			config:           config,
		},
		nil
}

// ReadK8sConfigFromPath Read the config file from path to be used in the kubeclient interface
func ReadK8sConfigFromPath() ([]byte, error) {
	path := getKubePath()
	config, err := ioutil.ReadFile(path)
	if err != nil {
		logging.Error("Error reading config from file")
		return nil, err
	}

	logging.Info("Kubernetes connection using KUBECONFIG: %s.\n", path)

	return config, nil
}

// getKubePath retrieves your kube config location
// This can be overwritten by setting KUBECONFIG
func getKubePath() string {
	var path string

	path = os.Getenv("KUBECONFIG")
	if "" == path {
		var home = os.Getenv("HOME")
		if "" == home {
			home = os.Getenv("USERPROFILE")
		}

		path = filepath.Join(home, ".kube", "config")
	}

	return path
}
