---
title: Checks
path: "/checks"
specialPage: checks
checksPath: ../haven/cli/pkg/compliancy/static/checks.yaml
---

De [Haven standaard](/techniek) bestaat momenteel uit {{compliancy}} verplichte en {{suggested}} voorgestelde checks.

Met de [Haven Compliancy Checker](/techniek/compliancy-checker) wordt een potentiële Haven omgeving automatisch nagelopen langs deze checks om Haven Compliancy te valideren.
