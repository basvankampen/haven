# Cloud Filestore

Deploys a Cloud Filestore NFS server for `ReadWriteMany` access.

<!-- BEGIN TFDOC -->
## Variables

| name | description | type | required | default |
|---|---|:---: |:---:|:---:|
| cidr_range | /29 range for Filestore | <code title="">string</code> | ✓ |  |
| name | Filestore instance name | <code title="">string</code> | ✓ |  |
| network | Network for Filestore instance | <code title="">string</code> | ✓ |  |
| project_id | GCP project ID | <code title="">string</code> | ✓ |  |
| region | Region for Filestore instance | <code title="">string</code> | ✓ |  |
| share_name | Filestore instance share name | <code title="">string</code> | ✓ |  |

## Outputs

| name | description | sensitive |
|---|---|:---:|
| filestore_ip | None |  |
| filestore_share | None |  |
<!-- END TFDOC -->
