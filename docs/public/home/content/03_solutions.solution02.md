---
icon: recycle
---

### Makkelijk herbruikbaar

Haven schrijft een specifieke configuratie voor van Kubernetes, bestaande technologie die wereldwijd bij duizendend organisaties in gebruik is.

Softwareontwikkelaars kunnen makkelijker applicaties voor verschillende organisaties ontwikkelen. Deze applicaties zijn eenvoudig herbruikbaar voor organisaties die Haven gebruiken.
