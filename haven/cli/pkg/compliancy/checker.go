// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package compliancy

import (
	"context"
	"fmt"
	"math/rand"
	"regexp"
	"strings"
	"time"

	"github.com/Masterminds/semver/v3"
	"github.com/gookit/color"
	"github.com/olekukonko/tablewriter"
	extensionsclient "k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/models/crds/haven/v1alpha"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

// Checker runs the compliancy checks and returns a list of results
type Checker struct {
	config   *Config
	platform v1alpha.Platform
}

type Platform string

const (
	PlatformUnknown   Platform = "Unknown"
	PlatformEKS       Platform = "Amazon AWS EKS"
	PlatformAKS       Platform = "Microsoft Azure AKS"
	PlatformGKE       Platform = "Google Cloud Platform GKE"
	PlatformOpenShift Platform = "Red Hat OpenShift"
	PlatformARO       Platform = "Azure Red Hat OpenShift (managed service)"
	AksImageLabel     string   = "kubernetes.azure.com/node-image-version"
	AksRole           string   = "kubernetes.azure.com/role"
	AksAgent          string   = "agent"
	EksKey            string   = "eks"
	GkeKey            string   = "gke"
)

// NewChecker returns a new Checker
func NewChecker(kubeConfig *rest.Config, kubeLatest string, cncfConfigured bool, runCISChecks bool) (*Checker, error) {
	platform := v1alpha.Platform{}

	kubeClient, err := kubernetes.NewForConfig(kubeConfig)
	if err != nil {
		return nil, err
	}

	extensionsClient, err := extensionsclient.NewForConfig(kubeConfig)
	if err != nil {
		return nil, err
	}

	kubeServerSrc, err := kubeClient.Discovery().ServerVersion()
	if err != nil {
		return nil, err
	}

	kubeServer, err := semver.NewVersion(kubeServerSrc.String())
	if err != nil {
		return nil, fmt.Errorf("Kubernetes server version: %s, Error: %s", kubeServerSrc.String(), err)
	}

	determindedPlatform, err := BestEffortPlatformCheck(kubeConfig, kubeClient, &platform)
	if err != nil {
		return nil, err
	}

	platform.DeterminedPlatform = string(determindedPlatform)

	return &Checker{
		config: &Config{
			KubeConfig:       kubeConfig,
			KubeClient:       kubeClient,
			KubeServer:       kubeServer,
			KubeLatest:       kubeLatest,
			ExtensionsClient: extensionsClient,
			CNCFConfigured:   cncfConfigured,
			RunCISChecks:     runCISChecks,
			HostPlatform:     determindedPlatform,
		},
		platform: platform,
	}, nil
}

func BestEffortPlatformCheck(kubeConfig *rest.Config, kubeClient *kubernetes.Clientset, platform *v1alpha.Platform) (Platform, error) {
	determinedPlatform := PlatformUnknown

	awsMasterPattern := regexp.MustCompile(`https:\/\/.*.eks.amazonaws.com`)
	if awsMasterPattern.MatchString(kubeConfig.Host) {
		determinedPlatform = PlatformEKS
	}

	azureMasterPattern := regexp.MustCompile(`https:\/\/.*.azmk8s.io`)
	if azureMasterPattern.MatchString(kubeConfig.Host) {
		determinedPlatform = PlatformAKS
	}

	serverVersion, err := kubeClient.DiscoveryClient.ServerVersion()
	if err != nil {
		return determinedPlatform, fmt.Errorf("Error determining server version: %s", err)
	}

	googleMasterVersionPattern := regexp.MustCompile(`gke`)
	if googleMasterVersionPattern.MatchString(serverVersion.String()) {
		determinedPlatform = PlatformGKE
	}

	namespaces, err := kubeClient.CoreV1().Namespaces().List(context.Background(), metav1.ListOptions{})
	if err == nil {
		// Loops over all namespaces and sets plaform to PlatformOpenShift if
		// openshift-apiserver namespace is found.
		for _, ns := range namespaces.Items {
			if "openshift-apiserver" == ns.Name {
				determinedPlatform = PlatformOpenShift
			}
		}
		// Loops over all namespaces again and overrides platform to PlatformARO if
		// openshift-azure-logging namespace is found.
		for _, ns := range namespaces.Items {
			if "openshift-azure-logging" == ns.Name {
				determinedPlatform = PlatformARO
			}
		}
	}

	// if the platform is still not known we try to get it by viewing the labels attached to a node
	// this is used primarily when there is a platform behind a proxy such as rancher or pinniped
	if determinedPlatform == PlatformUnknown {
		ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
		defer cancel()

		nodes, err := kubeClient.CoreV1().Nodes().List(ctx, metav1.ListOptions{})
		if err != nil {
			return determinedPlatform, err
		}

		randomNodeNumber := rand.Intn(len(nodes.Items))

		node := nodes.Items[randomNodeNumber]

		p, err := BestEffortLabelCheck(node.Labels, platform)
		if err != nil {
			return determinedPlatform, err
		}

		if p != PlatformUnknown {
			determinedPlatform = p
			platform.Proxy = true
		}
	}

	return determinedPlatform, nil
}

func BestEffortLabelCheck(labels map[string]string, platform *v1alpha.Platform) (Platform, error) {
	determinedPlatform := PlatformUnknown

	for key, value := range labels {
		if key == AksRole {
			if value == AksAgent {
				determinedPlatform = PlatformAKS
				platform.DeterminingLabel = fmt.Sprintf("%s=%s", key, value)
				break
			}
		}
		if key == AksImageLabel {
			match := AksBasedImage(value)
			if match {
				determinedPlatform = PlatformAKS
				platform.DeterminingLabel = fmt.Sprintf("%s=%s", key, value)
				break
			}
		}

		if strings.Contains(key, EksKey) {
			match := EksBasedLabel(key)
			if match {
				determinedPlatform = PlatformEKS
				platform.DeterminingLabel = fmt.Sprintf("%s=%s", key, value)
				break
			}
		}

		if strings.Contains(key, GkeKey) {
			match := GkeBasedLabel(key)
			if match {
				determinedPlatform = PlatformGKE
				platform.DeterminingLabel = fmt.Sprintf("%s=%s", key, value)
				break
			}
		}
	}

	return determinedPlatform, nil
}

func AksBasedImage(value string) bool {
	regex := "^AKSUbuntu[a-zA-Z0-9\\-\\.]+$"
	re := regexp.MustCompile(regex)
	match := re.MatchString(value)

	return match
}

func EksBasedLabel(value string) bool {
	regex := "^eks\\.amazonaws\\.com+\\/[a-zA-Z0-9\\-]+$"
	re := regexp.MustCompile(regex)
	match := re.MatchString(value)

	return match
}

func GkeBasedLabel(value string) bool {
	regex := "^cloud\\.google\\.com\\/gke-[a-zA-Z0-9\\-]+$"
	re := regexp.MustCompile(regex)
	match := re.MatchString(value)

	return match
}

// Run the checker and return a slice of results
func (c *Checker) Run() error {
	for i := range checks.CompliancyChecks {
		if !c.config.CNCFConfigured && checks.CompliancyChecks[i].Name == "cncf" {
			checks.CompliancyChecks[i].Result = ResultSkipped
			continue
		}

		compliancyResult, err := checks.CompliancyChecks[i].Exec(c.config)
		if err != nil {
			logging.Fatal("Compliancy Check '%s' interruption\n", checks.CompliancyChecks[i].Label)
		}

		checks.CompliancyChecks[i].Result = compliancyResult
	}

	for i := range checks.SuggestedChecks {
		if !c.config.RunCISChecks && checks.SuggestedChecks[i].Name == "cis" {
			checks.SuggestedChecks[i].Result = ResultSkipped
			continue
		}

		suggestedResult, err := checks.SuggestedChecks[i].Exec(c.config)
		if err != nil {
			logging.Fatal("Suggested Check '%s' interruption\n", checks.SuggestedChecks[i].Label)
		}

		checks.SuggestedChecks[i].Result = suggestedResult
	}

	return nil
}

// PrintResults prints the table of checks with the corresponding results
func (c *Checker) PrintResults(checks []Check, compliancyChecks bool) {
	tableOut := &strings.Builder{}
	table := tablewriter.NewWriter(tableOut)

	table.SetHeader([]string{"Category", "Name", "Passed"})
	table.SetAutoWrapText(false)

	totalChecks := 0
	unknownChecks := 0
	skippedChecks := 0
	failedChecks := 0
	passedChecks := 0

	for _, check := range checks {
		totalChecks++

		switch check.Result {
		case ResultUnknown:
			unknownChecks++
		case ResultSkipped:
			skippedChecks++
		case ResultNo:
			failedChecks++
		case ResultYes:
			passedChecks++
		}

		table.Append([]string{string(check.Category), check.Label, check.Result.String()})
	}

	table.Render()

	prefix := "Compliancy checks results:\n"
	if compliancyChecks {
		compliancy := fmt.Sprintf("Results: %d out of %d checks passed, %d checks skipped, %d checks unknown.", passedChecks, totalChecks, skippedChecks, unknownChecks)

		if passedChecks == totalChecks {
			logging.Info(color.Green.Sprintf("%s This is a Haven Compliant cluster.\n", compliancy))

			output.HavenCompliant = true
		} else if failedChecks > 0 {
			logging.Error(color.Red.Sprintf("%s This is NOT a Haven Compliant cluster.\n", compliancy))
		} else {
			logging.Warning(color.Yellow.Sprintf("%s This COULD be a Haven Compliant cluster.\n", compliancy))
		}

		output.CompliancyChecks.Summary.Total = totalChecks
		output.CompliancyChecks.Summary.Passed = passedChecks
		output.CompliancyChecks.Summary.Failed = failedChecks
		output.CompliancyChecks.Summary.Skipped = skippedChecks
		output.CompliancyChecks.Summary.Unknown = unknownChecks
	} else {
		prefix = "Suggested checks results:\n"
	}

	logging.Info(fmt.Sprintf("%s\n%s\n", prefix, tableOut.String()))
}
