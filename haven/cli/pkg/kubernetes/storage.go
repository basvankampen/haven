package kubernetes

import (
	"context"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"time"
)

// ListVolumesClaims gets a list of volumeclaim from a ns - empty string will be treated as all
func (k *KubeImpl) ListVolumesClaims(namespace string) (*corev1.PersistentVolumeClaimList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	pvcs, err := k.GetK8sClientSet().CoreV1().PersistentVolumeClaims(namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	return pvcs, nil
}

// DeleteVolumesClaim deletes a named volume claim
func (k *KubeImpl) DeleteVolumesClaim(namespace, name string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	err := k.GetK8sClientSet().CoreV1().PersistentVolumeClaims(namespace).Delete(ctx, name, metav1.DeleteOptions{})
	return err
}
