#   Copyright 2021 Google LLC
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

variable "project_id" {
  type        = string
  description = "GCP project ID"
}

variable "attestor_name" {
  type        = string
  description = "Attestor base name"
  default     = "haven-compliance"
}

variable "enforcement_mode" {
  type        = string
  description = "Enforcement mode"
  default     = "DRYRUN_AUDIT_LOG_ONLY"
}

variable "admission_allowlist" {
  type        = list(string)
  description = "Allowed containers"
  default     = ["docker.io/nginxinc/*"]
}

variable "compliance_testing_use" {
  type        = bool
  description = "Set to true if this cluster is being used for compliance testing"
  default     = true
}
