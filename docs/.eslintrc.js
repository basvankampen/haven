// Copyright © VNG Realisatie 2019-2021
// Licensed under the EUPL
//
module.exports = {
  plugins: ['react', 'prettier'],
  extends: ['@commonground/eslint-config-cra-standard-prettier'],
  settings: {
    react: {
      version: 'detect',
    },
  },
  rules: {
    'react/react-in-jsx-scope': 'off',
  },
}
