package certificates

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"errors"
	"math/big"
	"os"
	"path/filepath"
	"time"
)

type KeyGenerationInfo struct {
	CommonName   string
	Country      []string
	Province     []string
	Locality     []string
	Organization []string
}

//GenerateExternalCertificate creates a set of external certificates using golang's x509 package
// the output is a .key .csr file as bytes
func GenerateExternalCertificate(info KeyGenerationInfo) ([]byte, []byte) {
	keyBytes, _ := rsa.GenerateKey(rand.Reader, 4096)

	subject := pkix.Name{
		CommonName:   info.CommonName,
		Country:      info.Country,
		Province:     info.Province,
		Locality:     info.Locality,
		Organization: info.Organization,
	}

	template := x509.CertificateRequest{
		Subject:            subject,
		SignatureAlgorithm: x509.SHA256WithRSA,
	}

	csrBytes, _ := x509.CreateCertificateRequest(rand.Reader, &template, keyBytes)
	privBytes := x509.MarshalPKCS1PrivateKey(keyBytes)

	key := pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: privBytes})
	csr := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE REQUEST", Bytes: csrBytes})

	return key, csr
}

// GenerateKeyAndCertSet will create a .crt and .key file using golang's x509 package
// This means it is platform independent instead of having a macos and linux version (openssl@1.1 for mac)
func GenerateKeyAndCertSet(commonName string) ([]byte, []byte, error) {
	keyBytes, _ := rsa.GenerateKey(rand.Reader, 2048)

	ca := &x509.Certificate{
		SerialNumber: big.NewInt(2019),
		Subject: pkix.Name{
			CommonName: commonName,
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(0, 0, 3650),
		IsCA:                  true,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:              x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		BasicConstraintsValid: true,
	}
	caBytes, err := x509.CreateCertificate(rand.Reader, ca, ca, &keyBytes.PublicKey, keyBytes)
	if err != nil {
		return nil, nil, err
	}

	caPEM := new(bytes.Buffer)
	err = pem.Encode(caPEM, &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: caBytes,
	})

	if err != nil {
		return nil, nil, err
	}

	caPrivKeyPEM := new(bytes.Buffer)
	err = pem.Encode(caPrivKeyPEM, &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(keyBytes),
	})

	if err != nil {
		return nil, nil, err
	}

	return caPEM.Bytes(), caPrivKeyPEM.Bytes(), nil
}

func SerialNumberFromCert(certCRT string) (string, error) {
	block, _ := pem.Decode([]byte(certCRT))
	if block == nil {
		return "", errors.New("failed to parse certificate PEM")
	}
	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return "", err
	}

	return cert.Subject.SerialNumber, nil
}

func WriteKeyToFile(name, dirPath string, keyData []byte) error {
	path := filepath.Join(dirPath, name)
	err := os.WriteFile(path, keyData, 0600)
	if err != nil {
		return err
	}

	return nil
}
