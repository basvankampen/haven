---
newsLink1Text: Technische documentatie
newsLink1Href: /techniek
newsLink2Text: Neem contact op
newsLink2Href: /contact
---

## Aan de slag met Haven?

In onze technische documentatie staat beschreven hoe u Haven kunt installeren op uw huidige IT infrastructuur. Of neem contact met ons op, we helpen u graag op weg!
