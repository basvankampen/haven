#   Copyright 2021 Google LLC
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

terraform {
  required_version = ">= 1.0.0"

  required_providers {
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 4.0.0"
    }
    time = {
      source  = "hashicorp/time"
      version = ">= 0.7.0"
    }
  }
}

# Add Anthos Policy Controller
resource "google_gke_hub_membership" "anthos-membership" {
  provider = google-beta
  project  = var.project_id

  membership_id = format("membership-hub-%s", var.cluster_name)
  endpoint {
    gke_cluster {
      resource_link = format("//container.googleapis.com/%s", var.cluster_id)
    }
  }
}

# If you are getting an error from this, it means you already have the 
# feature enabled. Change the count to 0 to disable and remove the depends_on
# from below.
resource "google_gke_hub_feature" "anthos-acm-feature" {
  count    = var.enable_anthos_features ? 1 : 0
  provider = google-beta
  project  = var.project_id

  name     = "configmanagement"
  location = "global"
}

resource "google_gke_hub_feature_membership" "anthos-feature-member" {
  provider = google-beta
  project  = var.project_id

  location   = "global"
  feature    = "configmanagement"
  membership = google_gke_hub_membership.anthos-membership.membership_id
  configmanagement {
    version = "1.9.0"
    policy_controller {
      enabled                    = true
      template_library_installed = true
      referential_rules_enabled  = true
    }
  }

  depends_on = [
    google_gke_hub_feature.anthos-acm-feature
  ]
}

# Wait for Anthos Policy Controller to be deployed
resource "time_sleep" "wait-for-anthos-policy-controller" {
  depends_on = [google_gke_hub_feature_membership.anthos-feature-member]

  create_duration = "600s"
}

# Install CIS policies
resource "helm_release" "anthos-policy-controller-cis" {
  count = var.install_cis_policies ? 1 : 0

  name = "anthos-policy-controller-cis"

  chart = format("%s/../../cis-chart", path.module)

  set {
    name  = "clusterAdminUsers"
    value = format("{%s,%s,%s}", var.admin_email, var.acm_service_account, var.gkehub_service_account)
  }

  depends_on = [
    time_sleep.wait-for-anthos-policy-controller
  ]
}

