package crd

import (
	"context"
	"time"

	rbacapi "k8s.io/api/rbac/v1"
	apiextensionv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	"k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/kubernetes"
	rbachelper "k8s.io/kubernetes/pkg/apis/rbac/v1"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/models/crds/haven/v1alpha"
)

const (
	GroupName string = "haven.commonground.nl"
	Plural    string = "compliancies"
	Name             = Plural + "." + GroupName
)

// CreateOrRetrieveCRD retrieves the Haven CRD and will deploy it to the cluster when it's not installed yet.
func CreateOrRetrieveCRD(client *kubernetes.Clientset, apiextensionsclientset *clientset.Clientset) (bool, error) {
	created := false

	if !exists(apiextensionsclientset) {
		err := create(client, apiextensionsclientset)
		if err != nil {
			return false, err
		}

		created = true
	}

	return created, nil
}

func exists(apiextensionsclientset *clientset.Clientset) bool {
	_, err := apiextensionsclientset.ApiextensionsV1().CustomResourceDefinitions().Get(context.Background(), "compliancies.haven.commonground.nl", metav1.GetOptions{})

	return err == nil
}

func waitAccepted(apiextensionsclientset *clientset.Clientset) error {
	err := wait.Poll(1*time.Second, 30*time.Second, func() (bool, error) {
		crd, err := apiextensionsclientset.ApiextensionsV1().CustomResourceDefinitions().Get(context.TODO(), Name, metav1.GetOptions{})

		if err != nil {
			return false, err
		}

		for _, condition := range crd.Status.Conditions {
			if condition.Type == apiextensionv1.Established && condition.Status == apiextensionv1.ConditionTrue {
				return true, nil
			}
		}

		return false, nil
	})

	return err
}

func create(client *kubernetes.Clientset, apiextensionsclientset *clientset.Clientset) error {
	crd := v1alpha.CreateDefinition()

	_, err := apiextensionsclientset.ApiextensionsV1().CustomResourceDefinitions().Create(context.Background(), crd, metav1.CreateOptions{})
	if err != nil {
		return err
	}

	if err := waitAccepted(apiextensionsclientset); err != nil {
		return err
	}

	clusterrole := &rbacapi.ClusterRole{
		ObjectMeta: metav1.ObjectMeta{Name: "haven-compliancy-view"},
		Rules: []rbacapi.PolicyRule{
			rbachelper.NewRule("get", "list", "watch").Groups("haven.commonground.nl").Resources("compliancies").RuleOrDie(),
		},
	}

	clusterrolebinding := &rbacapi.ClusterRoleBinding{
		ObjectMeta: metav1.ObjectMeta{Name: "haven-compliancy-view-authenticated"},
		RoleRef:    rbacapi.RoleRef{Kind: "ClusterRole", Name: "haven-compliancy-view"},
		Subjects: []rbacapi.Subject{
			{Kind: "Group", Name: "system:authenticated"},
		},
	}

	if _, err = client.RbacV1().ClusterRoles().Create(context.TODO(), clusterrole, metav1.CreateOptions{}); err != nil {
		return err
	}

	if _, err = client.RbacV1().ClusterRoleBindings().Create(context.TODO(), clusterrolebinding, metav1.CreateOptions{}); err != nil {
		return err
	}

	return err
}
