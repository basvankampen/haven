#   Copyright 2021 Google LLC
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

variable "cluster_name" {
  type        = string
  description = "GKE cluster name"
}

variable "namespace" {
  type        = string
  description = "Namespace to install NFS provisioner in"
  default     = "kube-system"
}

variable "nfs_host" {
  type        = string
  description = "NFS server hostname or IP"
}

variable "nfs_share_path" {
  type        = string
  description = "NFS share path with leading path separator"
}

variable "path_pattern" {
  type        = string
  description = "Path pattern for workloads"
  default     = "$${.PVC.namespace}/$${.PVC.annotations.nfs.io/storage-path}"
}
