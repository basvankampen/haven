package kubernetes

import (
	"context"
	batchv1 "k8s.io/api/batch/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"time"
)

// ListJobs gets a list of jobs from a ns - empty string will be treated as all
func (k *KubeImpl) ListJobs(namespace string) (*batchv1.JobList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	jobs, err := k.GetK8sClientSet().BatchV1().Jobs(namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	return jobs, nil
}

func (k *KubeImpl) DeleteJob(namespace, jobName string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	err := k.GetK8sClientSet().BatchV1().Jobs(namespace).Delete(ctx, jobName, metav1.DeleteOptions{})
	return err
}
