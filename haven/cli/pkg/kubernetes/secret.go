package kubernetes

import (
	"context"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	corev1 "k8s.io/api/core/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"time"
)

func (k *KubeImpl) CreateSecret(namespace, secretName, typeAsString string, data map[string][]byte) error {
	var secretType corev1.SecretType

	switch typeAsString {
	case "tls":
		secretType = corev1.SecretTypeTLS
	case "docker":
		secretType = corev1.SecretTypeDockercfg
	default:
		secretType = corev1.SecretTypeOpaque
	}

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	_, err := k.GetSecret(namespace, secretName)

	if err != nil {
		if k8serrors.IsNotFound(err) {
			logging.Info("Secret %s not found, creating", secretName)
			secret := corev1.Secret{
				TypeMeta: metav1.TypeMeta{
					Kind:       "Secret",
					APIVersion: "v1",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name: secretName,
				},
				Immutable:  nil,
				Data:       data,
				StringData: nil,
				Type:       secretType,
			}
			_, err := k.GetK8sClientSet().CoreV1().Secrets(namespace).Create(ctx, &secret, metav1.CreateOptions{})
			if err != nil {
				return err
			}

			logging.Info("Created secret %s in namespace %s", secretName, namespace)
		}
	} else {
		logging.Info("Secret %s already exists in namespace %s", secretName, namespace)
	}

	return nil
}

func (k *KubeImpl) GetSecret(namespace, secretName string) (*corev1.Secret, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	secret, err := k.GetK8sClientSet().CoreV1().Secrets(namespace).Get(ctx, secretName, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	return secret, nil
}

func (k *KubeImpl) ListSecrets(namespace string) (*corev1.SecretList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	secrets, err := k.GetK8sClientSet().CoreV1().Secrets(namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	return secrets, nil
}

func (k *KubeImpl) DeleteSecret(namespace, secretName string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	err := k.GetK8sClientSet().CoreV1().Secrets(namespace).Delete(ctx, secretName, metav1.DeleteOptions{})

	return err
}
