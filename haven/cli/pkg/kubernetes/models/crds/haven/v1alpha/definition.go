package v1alpha

import (
	apiextensionv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	GroupName string = "haven.commonground.nl"
	Kind      string = "Compliancy"
	Version   string = "v1alpha1"
	Singular  string = "compliancy"
	ShortName string = "compliancy"
	Plural    string = "compliancies"
	Name             = Plural + "." + GroupName
)

// CreateDefinition creates a new CRD specific to haven
func CreateDefinition() *apiextensionv1.CustomResourceDefinition {
	crd := &apiextensionv1.CustomResourceDefinition{
		ObjectMeta: metav1.ObjectMeta{Name: Name},
		Spec: apiextensionv1.CustomResourceDefinitionSpec{
			Group: GroupName,
			Scope: apiextensionv1.ClusterScoped,
			Names: apiextensionv1.CustomResourceDefinitionNames{
				Plural:     Plural,
				Singular:   Singular,
				ShortNames: []string{ShortName},
				Kind:       Kind,
			},
			Versions: []apiextensionv1.CustomResourceDefinitionVersion{
				{
					Name:    Version,
					Served:  true,
					Storage: true,
					Schema: &apiextensionv1.CustomResourceValidation{
						OpenAPIV3Schema: &apiextensionv1.JSONSchemaProps{
							Type: "object",
							Properties: map[string]apiextensionv1.JSONSchemaProps{
								"spec": {
									Type: "object",
									Properties: map[string]apiextensionv1.JSONSchemaProps{
										"created":   {Type: "string"},
										"version":   {Type: "string"},
										"compliant": {Type: "boolean"},
										"input": {Type: "object",
											Properties: map[string]apiextensionv1.JSONSchemaProps{
												"Commandline": {Type: "string"},
												"KubeHost":    {Type: "string"},
												"Platform": {Type: "object",
													Properties: map[string]apiextensionv1.JSONSchemaProps{
														"Proxy":              {Type: "boolean"},
														"DeterminingLabel":   {Type: "string"},
														"DeterminedPlatform": {Type: "string"},
													},
												},
											},
										},
										"output": {
											Type: "object",
											Properties: map[string]apiextensionv1.JSONSchemaProps{
												"Version":        {Type: "string"},
												"HavenCompliant": {Type: "boolean"},
												"StartTS":        {Type: "string"},
												"StopTS":         {Type: "string"},
												"Config": {
													Type: "object",
													Properties: map[string]apiextensionv1.JSONSchemaProps{
														"CNCF": {Type: "boolean"},
														"CIS":  {Type: "boolean"},
													},
												},

												"CompliancyChecks": {
													Type: "object",
													Properties: map[string]apiextensionv1.JSONSchemaProps{
														"Results": {
															Type: "array",
															Items: &apiextensionv1.JSONSchemaPropsOrArray{
																Schema: &apiextensionv1.JSONSchemaProps{
																	Type: "object",
																	Properties: map[string]apiextensionv1.JSONSchemaProps{
																		"Name":      {Type: "string"},
																		"Label":     {Type: "string"},
																		"Category":  {Type: "string"},
																		"Rationale": {Type: "string"},
																		"Result":    {Type: "string"},
																	},
																},
															},
														},

														"Summary": {Type: "object",
															Properties: map[string]apiextensionv1.JSONSchemaProps{
																"Total":   {Type: "number"},
																"Unknown": {Type: "number"},
																"Skipped": {Type: "number"},
																"Failed":  {Type: "number"},
																"Passed":  {Type: "number"},
															},
														},
													},
												},
												"SuggestedChecks": {
													Type: "object",
													Properties: map[string]apiextensionv1.JSONSchemaProps{
														"Results": {
															Type: "array",
															Items: &apiextensionv1.JSONSchemaPropsOrArray{
																Schema: &apiextensionv1.JSONSchemaProps{
																	Type: "object",
																	Properties: map[string]apiextensionv1.JSONSchemaProps{
																		"Name":      {Type: "string"},
																		"Label":     {Type: "string"},
																		"Category":  {Type: "string"},
																		"Rationale": {Type: "string"},
																		"Result":    {Type: "string"},
																	},
																},
															},
														},
													},
												},
											},
										},
									},
									Required: []string{
										"created",
										"version",
										"compliant",
									},
								},
							},
						},
					},
					AdditionalPrinterColumns: []apiextensionv1.CustomResourceColumnDefinition{
						{
							Name:     "version",
							Type:     "string",
							JSONPath: ".spec.version",
						},
						{
							Name:     "compliant",
							Type:     "boolean",
							JSONPath: ".spec.compliant",
						},
					},
				},
			},
		},
	}

	return crd
}
