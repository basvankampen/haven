// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package compliancy

import (
	"bytes"
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/tools/remotecommand"
	"k8s.io/kubectl/pkg/scheme"
)

// RunCommandOnPrivilegedPod takes a string as command input and returns the output from
// running that command from a privileged pod, unless this results in an error.
// It's possible to force scheduling of the pod on a master node when needed.
func RunCommandOnPrivilegedPod(config *Config, useMaster bool, failOnExecErr bool, cmd string) (string, error) {
	// Find a master node to work with.
	nodes, err := config.KubeClient.CoreV1().Nodes().List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return "", err
	}

	var master apiv1.Node

	if useMaster {
		for _, n := range nodes.Items {
			if n.Labels["kubernetes.io/role"] == "master" {
				master = n
				break
			}

			if _, exists := n.Labels["node-role.kubernetes.io/control-plane"]; exists {
				master = n
				break
			}

			if _, exists := n.Labels["node-role.kubernetes.io/controlplane"]; exists {
				master = n
				break
			}

			if _, exists := n.Labels["node-role.kubernetes.io/master"]; exists {
				master = n
				break
			}

			if _, exists := n.Labels["node.kubernetes.io/master"]; exists {
				master = n
				break
			}
		}
		if master.Name == "" {
			return "", errors.New("Could not find any master node to run privileged pod on")
		}
	}

	// Create privileged pod. See https://miminar.fedorapeople.org/_preview/openshift-enterprise/registry-redeploy/go_client/executing_remote_processes.html.
	app := "hcc-test-" + strings.ToLower(base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(time.Now().Nanosecond())))[:5])
	ns := "default"

	var priv bool = true

	var tolerations []apiv1.Toleration

	// Define tolerations if pod needs to be deployed on master/controlplane node.
	if useMaster {
		for _, taint := range master.Spec.Taints {
			tolerations = append(tolerations,apiv1.Toleration{
				Key: 		taint.Key,
				Operator: 	apiv1.TolerationOpExists,
				Effect: 	taint.Effect,
			})
		}
	}

	pod, err := config.KubeClient.CoreV1().Pods(ns).Create(context.Background(), &apiv1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name: app,
		},
		Spec: apiv1.PodSpec{
			Containers: []apiv1.Container{
				{
					Name:    app,
					Image:   "busybox",
					Command: []string{"cat"},
					Stdin:   true,
					SecurityContext: &apiv1.SecurityContext{
						Privileged: &priv,
					},
				},
			},
			HostPID:  true,
			NodeName: master.Name,
			Tolerations: tolerations,
		},
	}, metav1.CreateOptions{})
	if err != nil {
		return "", err
	}

	defer func() {
		if err := config.KubeClient.CoreV1().Pods(pod.Namespace).Delete(context.Background(), pod.Name, metav1.DeleteOptions{}); err != nil {
			logging.Error("Could not cleanup privileged pod: '%s'", err.Error())
		}
	}()

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	// Wait for 60sec for pod to become ready.
	ticker := time.NewTicker(1 * time.Second)
	timeout := time.After(60 * time.Second)
	var podHealthy bool

	for {
		select {
		case <-ticker.C:
			po, err := config.KubeClient.CoreV1().Pods(pod.Namespace).Get(ctx, pod.Name, metav1.GetOptions{})
			if err != nil {
				continue
			}

			if po.Status.Phase != apiv1.PodRunning {
				continue
			}

			podHealthy = true
			ticker.Stop()

		case <-timeout:
			logging.Error("timed out")
			ticker.Stop()
		}
		break
	}

	if !podHealthy {
		return "", fmt.Errorf("busybox pod has not become healthy after 60 seconds")
	}

	// Run command.
	req := config.KubeClient.CoreV1().RESTClient().
		Post().
		Resource("pods").
		Namespace(pod.Namespace).
		Name(pod.Name).
		SubResource("exec").
		VersionedParams(&apiv1.PodExecOptions{
			Container: pod.Spec.Containers[0].Name,
			Command:   []string{"nsenter", "--target", "1", "--mount", "--uts", "--ipc", "--net", "--pid", "--", "sh", "-c", cmd},
			Stdin:     false,
			Stdout:    true,
			Stderr:    true,
			TTY:       false,
		}, scheme.ParameterCodec)

	exec, err := remotecommand.NewSPDYExecutor(config.KubeConfig, "POST", req.URL())
	if err != nil {
		return "", err
	}

	var stdout, stderr bytes.Buffer
	err = exec.Stream(remotecommand.StreamOptions{
		Stdin:  nil,
		Stdout: &stdout,
		Stderr: &stderr,
		Tty:    false,
	})
	if err != nil {
		if failOnExecErr {
			return "", err
		}
	}

	output := stdout.String()
	if len(stderr.String()) != 0 {
		output += "\n" + stderr.String()
	}

	return output, nil
}
