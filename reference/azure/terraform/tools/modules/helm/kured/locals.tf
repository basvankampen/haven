locals {
  namespace = "kured"
  kured_default_values = {
    "image.repository"         = "weaveworks/kured"
    "image.tag"                = "1.5.0"
    "image.pullPolicy"         = "IfNotPresent"
    "extraArgs.reboot-days"    = "tue"
    "extraArgs.start-time"     = "2am"
    "extraArgs.end-time"       = "6am"
    "extraArgs.time-zone"      = "UTC"
    "rbac.create"              = "true"
    "podSecurityPolicy.create" = "false"
    "serviceAccount.create"    = "true"
  }

  kured_values = merge(local.kured_default_values, var.kured_settings)
}
