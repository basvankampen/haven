package handler

import (
	"bytes"
	"net/http"
	"net/url"
)

const jsonContentType = "application/json"

func PostRequest(u url.URL, body []byte, contentType string) (*http.Response, error) {
	if contentType == "" {
		contentType = jsonContentType
	}
	req, _ := http.NewRequest(http.MethodPost, u.String(), bytes.NewBuffer(body))
	req.Header.Set("Content-Type", contentType)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func GetRequest(u url.URL) (*http.Response, error) {
	req, _ := http.NewRequest(http.MethodGet, u.String(), nil)
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	return resp, nil
}
