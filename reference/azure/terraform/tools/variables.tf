variable "kubeconfig" {
  description = "kubeconfig"
  default     = null
}

variable "cert_manager" {
  description = "cert-manager settings"
  default = {
    enabled          = true
    settings         = {}
    namespace        = "cert-manager"
    chart_repository = "https://charts.jetstack.io"
    chart_version    = "v0.13.0"
  }
}

variable "kured" {
  description = "Kured add-on. https://docs.microsoft.com/en-us/azure/aks/node-updates-kured"
  default = {
    enabled = true
    settings = {
      "image.tag" = "1.5.0"
    }
    namespace        = "kured"
    chart_repository = "https://weaveworks.github.io/kured"
    chart_version    = "2.2.0"
  }
}


variable "fluentd" {
  description = "fluentd"
  default = {
    enabled          = true
    settings         = {}
    namespace        = "fluentd"
    chart_repository = "https://fluent.github.io/helm-charts"
    chart_version    = "0.2.2"
  }
}
