provider "aws" {
  region = var.region
}

resource "aws_kms_key" "haven" {
  description = var.clustername
}

resource "aws_kms_alias" "haven" {
  name          = "alias/haven"
  target_key_id = aws_kms_key.haven.key_id
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name            = var.clustername
  cidr            = var.vpc_cidr
  azs             = var.availability_zones
  private_subnets = var.private_subnets
  public_subnets  = var.public_subnets

  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    "kubernetes.io/cluster/${var.clustername}" = "shared"
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/${var.clustername}" = "shared"
    "kubernetes.io/role/elb"                   = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${var.clustername}" = "shared"
    "kubernetes.io/role/internal-elb"          = "1"
  }
}

module "eks" {
  source = "terraform-aws-modules/eks/aws"

  cluster_name    = var.clustername
  cluster_version = "1.18"

  vpc_id  = module.vpc.vpc_id
  subnets = module.vpc.private_subnets

  cluster_endpoint_private_access      = var.endpoint_private_access
  cluster_endpoint_public_access       = var.endpoint_public_access
  cluster_endpoint_public_access_cidrs = var.endpoint_public_access_cidrs

  cluster_encryption_config = [
    {
      provider_key_arn = aws_kms_key.haven.arn
      resources        = ["secrets"]
    }
  ]

  worker_groups = [
    {
      name                 = "default"
      instance_type        = var.instance_type
      asg_desired_capacity = var.scaling_config_desired_size
    }
  ]
}
