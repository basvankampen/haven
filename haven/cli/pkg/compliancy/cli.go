// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package compliancy

import (
	"embed"
	"encoding/json"
	"fmt"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/models/crds/haven/v1alpha"
	"io/ioutil"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"os"
	"os/exec"
	"strings"
	"time"

	cli "github.com/jawher/mow.cli"
	"sigs.k8s.io/yaml"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/crd"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

var (
	kubeLatest     = "undefined"
	callingCommand = ""
)

// CLIConfig renders the command line function
var CLIConfig = func(config *cli.Cmd) {
	rationale := config.BoolOpt("rationale", false, "[Compliancy] Displays the reasoning behind all Compliancy Checks (does not run the Checker).")
	runCNCFChecks := config.BoolOpt("cncf", true, "[Compliancy] Run external CNCF checks. Required, results in skipped test when set to false.")
	runCISChecks := config.BoolOpt("cis", false, "[Suggestion] Run external CIS checks by specifying log output file path. Optional.")

	args := os.Args
	callingCommand = strings.Join(args, " ")
	config.Action = func() {
		if err := initChecker(); err != nil {
			fmt.Printf("Fatal error initializing Haven Compliancy Checker: %s.\n", err.Error())

			return
		}

		if *rationale {
			cmdRationale()

			return
		}

		cmdCheck(runCNCFChecks, runCISChecks)
	}
}

var (
	Version string

	checks    Checks
	output    CompliancyOutput
	rationale RationaleOutput

	cncfConfigured bool = false

	//go:embed static/checks.yaml
	embedFiles embed.FS
)

// initChecker sources checks.yaml and preps output.
func initChecker() error {
	rationale.Version = fmt.Sprintf("Haven %s", Version)
	output.Version = fmt.Sprintf("Haven %s", Version)
	output.StartTS = time.Now()

	f, err := embedFiles.Open("static/checks.yaml")
	if err != nil {
		return err
	}

	defer f.Close()

	b, err := ioutil.ReadAll(f)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(b, &checks)
	if err != nil {
		return err
	}

	return nil
}

// cmdCheck validates requirements, runs the in scope checks and outputs
// results.
func cmdCheck(runCNCFChecks *bool, runCISChecks *bool) {
	logging.Debug("HAVEN COMPLIANCY CHECKER - STARTED\n")
	defer logging.Debug("HAVEN COMPLIANCY CHECKER - FINISHED\n\n\n")

	kubePath, _ := kubernetes.ReadK8sConfigFromPath()
	kube, err := kubernetes.New(kubePath)
	if err != nil {
		logging.Fatal("Error creating kubeClient: %s\n", err)
	}

	kubeConfig := kube.RestConfig

	if *runCNCFChecks {
		if path, err := exec.LookPath("sonobuoy"); err == nil {
			cncfConfigured = true

			logging.Info("CNCF: Sonobuoy binary found at '%s'. Enabled external CNCF compliancy check.\n", path)
		} else {
			logging.Warning("CNCF: Sonobuoy binary not found! Skipping external CNCF compliancy check.\n")
		}
	} else {
		logging.Warning("CNCF: Opted out! Skipping external CNCF compliancy check.\n")
	}

	output.Config.CNCF = cncfConfigured

	if *runCISChecks {
		logging.Info("CIS: Opted in. Enabled external CIS suggested check.\n")
	} else {
		logging.Info("CIS: Not opted in. Skipping external CIS suggested check.\n")
	}

	output.Config.CIS = *runCISChecks

	logging.Info("Initializing")

	checker, err := NewChecker(kubeConfig, kubeLatest, cncfConfigured, *runCISChecks)
	if err != nil {
		logging.Fatal("Could not initialize checker: %s\n", err.Error())
	}

	logging.Info("Kubernetes host platform: %s", checker.config.HostPlatform)

	logging.Info("Detecting Haven CRD")
	created, err := crd.CreateOrRetrieveCRD(checker.config.KubeClient, checker.config.ExtensionsClient)
	if err != nil {
		logging.Fatal("Error occured while detecting or deploying Haven CRD: %s", err.Error())
	}
	if created {
		logging.Info("Haven CRD was not installed yet and has now been deployed")
	}

	logging.Info("Running checks...")
	err = checker.Run()
	if err != nil {
		logging.Error("Checker encountered an error: %s\n", err.Error())
		return
	}

	output.CompliancyChecks.Results = checks.CompliancyChecks
	output.SuggestedChecks.Results = checks.SuggestedChecks
	output.StopTS = time.Now()

	checker.PrintResults(checks.CompliancyChecks, true)
	checker.PrintResults(checks.SuggestedChecks, false)

	oj, err := outputJson(output)
	if err != nil {
		logging.Error("Could build JSON output: %s", err)

		if *logging.OutputFormat == "json" {
			type jsonerr struct {
				Error string `json:",omitempty"`
			}
			r, _ := json.Marshal(jsonerr{Error: err.Error()})
			oj = fmt.Sprint(string(r))

			fmt.Println(oj)
		}
	} else {
		if *logging.OutputFormat == "json" {
			fmt.Println(oj)
		}
	}

	compliant := output.CompliancyChecks.Summary.Total == output.CompliancyChecks.Summary.Passed

	input := v1alpha.Input{
		TypeMeta:    metav1.TypeMeta{},
		ObjectMeta:  metav1.ObjectMeta{},
		Commandline: callingCommand,
		KubeHost:    kubeConfig.Host,
		Platform:    checker.platform,
	}

	if err := persistInCluster(checker.config.KubeConfig, compliant, oj, input); err != nil {
		logging.Error(err.Error())
	}
}

// cmdRationale outputs a table based on checks.yaml with all Compliancy and Suggested checks
// with the reasoning behind these checks.
func cmdRationale() {
	logging.Info("The Haven Compliancy Checker contains %d required Compliancy Checks to validate a cluster.", len(checks.CompliancyChecks))
	logging.Info("Compliancy Checks are restricted to essentials allowing for maximum flexibility while keeping the Haven promise of interoperability.")
	logging.Info("Besides the required checks there are %d Suggested Checks which can assist further enhancement of Haven environments.\n\n\n", len(checks.SuggestedChecks))

	logging.Info("** Compliancy Checks **\n\n")
	for _, c := range checks.CompliancyChecks {
		logging.Info(fmt.Sprintf("%s\n%s\n\n", c.Label, c.Rationale))
	}

	logging.Info("** Suggested Checks **\n\n")
	for _, s := range checks.SuggestedChecks {
		logging.Info(fmt.Sprintf("%s\n%s\n\n", s.Label, s.Rationale))
	}

	if *logging.OutputFormat == "json" {
		rationale.CompliancyChecks = checks.CompliancyChecks
		rationale.SuggestedChecks = checks.SuggestedChecks

		oj, err := outputJson(rationale)
		if err != nil {
			logging.Error("Could not print JSON output: %s", err)
		} else {
			fmt.Println(oj)
		}
	}
}
