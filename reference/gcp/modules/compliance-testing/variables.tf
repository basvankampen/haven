#   Copyright 2021 Google LLC
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

variable "project_id" {
  type        = string
  description = "GCP project ID"
}

variable "network_name" {
  type        = string
  description = "VPC name"
}

variable "cidr_range_controlplane" {
  type        = string
  description = "GKE control plane range (/28)"
}

variable "admin_email" {
  type        = string
  description = "Deployment user email or service account email"
  default     = ""
}

variable "compliance_testing_use" {
  type        = bool
  description = "Set to true if this cluster is being used for compliance testing"
  default     = true
}

variable "kubernetes_endpoint" {
  type        = string
  description = "Kubernetes endpoint for patching"
  default     = ""
}

variable "ca_certificate" {
  type        = string
  description = "Control panel CA certificate"
  default     = ""
}

variable "access_token" {
  type        = string
  description = "Access token"
  default     = ""
}

